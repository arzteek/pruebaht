# API Restframework Propiedades

_Lista, guarda y crea usarios desde la api_


### Requisitos
```
Python 3
Pip
Cmake
```


### Instalación
* Clona o descarga el proyecto
* Ingresa a la carpeta raiz del proyecto del proyecto

_En consola_

```
# pip install virtualenv

# virtualenv my-env -p python3
# source my-env/bin/activate
# pip install -r requirements.txt
# make start-dev
```


### Ejemplo de uso
Inicia la aplicación 
Para ver los servicios disponibles y probarlos ingresa a (http://editor.swagger.io/) , copia y pega el swagger.yaml que incluye el proyecto.


En la interfaz web apareceran cada solicitud recibida