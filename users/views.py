from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_201_CREATED
)
from rest_framework.response import Response
from django.contrib.auth.models import User
from .serializers import  UserLoginSerializer, UserCreateSerializer
from .default_authentication import ExpiringTokenAuthentication

@api_view(["POST"])
@permission_classes((AllowAny,))  
def login(request):
    signin_serializer = UserLoginSerializer(data = request.data)
    if not signin_serializer.is_valid():
        return Response(signin_serializer.errors, status = HTTP_400_BAD_REQUEST)


    user = authenticate(
            username = signin_serializer.data['username'],
            password = signin_serializer.data['password'] 
        )
    if not user:
        return Response({'detail': 'Credenciales invalidas o usuario desactivado'}, status=HTTP_404_NOT_FOUND)

    token, _ = Token.objects.get_or_create(user = user)
    
    
    _, token = ExpiringTokenAuthentication.is_token_expired(token) 
   
    return Response({
        'token': token.key
    }, status=HTTP_200_OK)

@api_view(["POST"])
@permission_classes((AllowAny,))  
def crear_usuario(request):
    create_serializer = UserCreateSerializer(data = request.data)

    if not create_serializer.is_valid():
        return Response(create_serializer.errors, status = HTTP_400_BAD_REQUEST)
    

    user = User.objects.create_user(
        username = create_serializer.data["username"],
        email = create_serializer.data["email"],
        password = create_serializer.data["password"]
    )

    token, _ = Token.objects.get_or_create(user=user)
    
    return Response({
        'username': user.username,
        'token': token.key
    }, status=HTTP_201_CREATED)


   
