from django.contrib import admin
from django.urls import path
from .views import login, crear_usuario


urlpatterns = [
    path('login', login),
    path('registrar', crear_usuario)
]
