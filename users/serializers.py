from rest_framework import serializers
from django.contrib.auth.models import User

class UserLoginSerializer(serializers.Serializer):
    """
    Valida los datos de usuario para login
    """
    username = serializers.CharField(required = True)
    password = serializers.CharField(required = True)


class UserCreateSerializer(serializers.Serializer):
    """
    valida los datos para crear un nuevo usuario
    """
    username = serializers.CharField(required = True)
    password = serializers.CharField(required = True)
    email = serializers.EmailField(required = True)