from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import AuthenticationFailed

from datetime import timedelta
from django.utils import timezone
from django.conf import settings

class ExpiringTokenAuthentication(TokenAuthentication):
    def authenticate_credentials(self, key):
        """
        verifica si las credenciales de autorizacion de un usuario (token)
        no 
        """
        try:
            token = Token.objects.get(key = key)
        except Token.DoesNotExist:
            raise AuthenticationFailed("Token invalido")
        
        if not token.user.is_active:
            raise AuthenticationFailed("Usuario inactivo")

        is_expired, token = self.is_token_expired(token)
        if is_expired:
            raise AuthenticationFailed("El token ha expirado")
        
        return (token.user, token)
    
 
    @classmethod
    def is_token_expired(cls, token):
        """
        Verifica si el token dado ya expiro o sigue vigente
        """
        is_expired = cls.expires_in(token) < timedelta(seconds = 0)
    
        return is_expired, token 

    @classmethod
    def expires_in(cls, token):
        """
        Obitene la fecha en la que un token vence
        """
        time_elapsed = timezone.now() - token.created

        left_time = timedelta(seconds = settings.LIFE_TOKEN_SECONDS) - time_elapsed
       
        return left_time