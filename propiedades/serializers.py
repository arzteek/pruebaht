from .models import Propiedad
from rest_framework import serializers


class PropiedadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Propiedad
        fields = ("id","nombre", "direccion", "m2", "email", )