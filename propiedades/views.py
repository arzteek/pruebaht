from rest_framework import viewsets
from rest_framework.response import Response


from .models import Propiedad
from .serializers import PropiedadSerializer

class PropiedadView(viewsets.ModelViewSet):
   
    queryset =  Propiedad.objects.all()
    serializer_class = PropiedadSerializer
  

  