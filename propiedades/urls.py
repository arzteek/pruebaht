from rest_framework import routers
from django.urls import path
from .views import PropiedadView

router = routers.SimpleRouter()
router.register('propiedades', PropiedadView)

urlpatterns = router.urls