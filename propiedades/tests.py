import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from .models import Propiedad


# initialize the APIClient app
client = Client()


class GetAllPropiedades(TestCase):
    def setUp(self):
        Propiedad.objects.create(nombre="Propiedad 1", direccion="DIreccion 1", m2 = 343, email = "propiedad1@gmail.com")
        Propiedad.objects.create(nombre="Propiedad 2", direccion="DIreccion 2", m2 = 343, email = "propiedad2@gmail.com")
        Propiedad.objects.create(nombre="Propiedad 4", direccion="DIreccion 4", m2 = 343, email = "propiedad4@gmail.com")
        Propiedad.objects.create(nombre="Propiedad 5", direccion="DIreccion 5", m2 = 343, email = "propiedad5@gmail.com")
    
    def test_get_all_propiedades(self):
        pass
