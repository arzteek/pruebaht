from __future__ import unicode_literals
from django.db  import models
from django.core.validators import MaxValueValidator

class Propiedad(models.Model):
    nombre = models.CharField(max_length=50)
    direccion = models.CharField(max_length=100)
    m2 = models.PositiveIntegerField(validators=[MaxValueValidator(99999)])
    email = models.EmailField(max_length=50)