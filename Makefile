#################################
#
#  COMMANDS DEVELOP
#
################################
start-dev:
	python manage.py runserver

create-migrations:
	python manage.py makemigrations

migrate:
	python manage.py migrate

shell:
	python manage.py shell